﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBird : Bird
{
    public float explodeRadius;
    public float explodeForce;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Explode();
        Debug.Log("meledak");
    }

    private void Explode()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explodeRadius);
        foreach (Collider2D nearbyObject in colliders)
        {
            Vector2 direction = nearbyObject.transform.position - transform.position;
            Rigidbody2D rb = nearbyObject.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rb.AddForce(direction * explodeForce);
            }
        }
        Destroy(gameObject);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explodeRadius);
    }
}